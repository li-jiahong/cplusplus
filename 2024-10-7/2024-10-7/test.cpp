#include "Date.h"

//日期类的实现

int main()
{
	Date d1;
	cout << "d1:";
	d1.Print();

	printf("\n");

	Date d2(2024, 10, 8);
	//Date d3 = d2;
	Date d3(d2);
	cout << "d3:";
	d3.Print();

	printf("\n");

	Date d4;
	d4 = d3 = d1;
	cout << "d4:";
	d4.Print();

	printf("\n");

	Date d5(2024, 10, 7);
	d5 += 100;
	d5.Print();

	printf("\n");

	Date d6(2024,10,7);
	Date d7(2024, 10, 7);
	d6 + 100;
	d6.Print();
	d7 = d6 + 100;
	d7.Print();

	printf("\n");

	Date d8(2023,10,7);
	Date d9(2024,10,8);
	cout << (d8 >  d9) << endl;//0
	cout << (d8 >= d9) << endl;//0
	cout << (d8 <  d9) << endl;//1
	cout << (d8 <= d9) << endl;//1
	cout << (d8 == d9) << endl;//0
	cout << (d8 != d9) << endl;//1

	printf("\n");

	Date d10;
	Date d11;
	//d11 = ++d10;
	d11 = d10++;
	d10.Print();
	d11.Print();

	printf("\n");

	Date d12(2024,10,8);
	d12 -= 100;
	d12.Print();

	printf("\n");

	Date d13(2024,10,8);
	d13 -= -100;
	d13.Print();

	printf("\n");

	Date d14(2024, 10, 8);
	d14 += -100;
	d14.Print();

	printf("\n");

	Date d15(2024, 10, 8);
	d15 - 100;
	d15.Print();
	Date d16;
	d16 = d15 - 100;
	d16.Print();

	printf("\n");

	Date d17(2024, 10, 8);
	Date d18;
	d18 = d17 + -100;
	d18.Print();

	printf("\n");

	Date d19(2024, 10, 8);
	Date d20 = --d19;
	d19.Print();
	d20.Print();
	Date d21(2024, 10, 8);
	Date d22 = d21--;
	d21.Print();
	d22.Print();

	printf("\n");

	Date d23(2024, 10, 8);
	Date d24(1949, 10, 1);
	cout << (d23 - d24) << endl;

	return 0;
}