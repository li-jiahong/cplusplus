#include <iostream>
using namespace std;

//1. 初始化列表
//初始化列表：以一个冒号开始，接着是一个以逗号分隔的数据成员列表，
//每个"成员变量"后面跟一个放在括号中的初始值或表达式。
//class Date
//{
//public:
//	Date(int year,int month,int day)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{ 
//	
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//
//int main()
//{
//	Date d1(2024,10,13);
//
//
//	return 0;
//}

//【注意】
//1. 每个成员变量在初始化列表中最多只能出现一次(初始化只能初始化一次)
//2. 类中包含以下成员，必须放在初始化列表位置进行初始化：
//1)引用成员变量
//2)const成员变量
//3)自定义类型成员(且该类没有默认构造函数时)

//class A
//{
//public:
//	A(int a)
//		:_a(a)
//	{}
//private:
//	int _a;
//};
//class B
//{
//public:
//	B(int a, int ref)
//		:_aobj(a)
//		, _ref(ref)
//		, _n(10)
//	{}
//private:
//	A _aobj; // 没有默认构造函数
//	int& _ref; // 引用
//	const int _n; // const 
//};
//
//int main()
//{
//	B aa(1, 2);
//
//	return 0;
//}

//3) 尽量使用初始化列表初始化，因为不管你是否使用初始化列表，
//   对于自定义类型成员变量，一定会先使用初始化列表初始化。

//class Time
//{
//public:
//	Time(int hour = 0)
//		:_hour(hour)
//	{
//		cout << "Time()" << endl;
//	}
//private:
//	int _hour;
//};
//class Date
//{
//public:
//	Date(int day)
//		:_day(day)
//	{}
//private:
//	int _day;
//	Time _t;
//};
//
//int main()
//{
//	Date d(1);
//
//	return 0;
//}

//4) 成员变量在类中声明次序就是其在初始化列表中的初始化顺序，与其在初始化列表中的先后次序无关

//class A
//{
//public:
//	A(int a)
//		:_a1(a)
//		, _a2(_a1)
//	{}
//
//	void Print() {
//		cout << _a1 << " " << _a2 << endl;
//	}
//private:
//	int _a2;
//	int _a1;
//};
//int main() {
//	A aa(1);
//	aa.Print();
//
//	return 0;
//}

//2.static成员--用static修饰的成员函数，称之为静态成员函数。静态成员变量一定要在类外进行初始化
class A
{
public:
	A() { ++_scount; }
	A(const A& t) { ++_scount; }
	~A() { --_scount; }
	static int GetACount() { return _scount; }
private:
	static int _scount;
};
int A::_scount = 0;

int main()
{
	cout << A::GetACount() << endl;
	A a1, a2;
	A a3(a1);
	cout << A::GetACount() << endl;


	return 0;
}







