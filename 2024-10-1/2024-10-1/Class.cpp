#include "Class.h"


//成员函数定义放在.cpp文件中，注意：成员函数名前需要加类名::
void Stack::Init(int defaultcapacity)
{
	a = (int*)malloc(sizeof(int) * defaultcapacity);
	if (a == nullptr)
	{
		perror("malloc申请失败!\n");
		return;
	}
	top = 0;
	capacity = defaultcapacity;
}

