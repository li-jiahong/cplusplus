
//类的引入

//typedef int DataType;
////struct Stack
//class Stack
//{
//public:
//	//成员函数
//	void Init(size_t capacity)
//	{
//		_array = (DataType*)malloc(sizeof(DataType) * capacity);
//		if (nullptr == _array)
//		{
//			perror("malloc申请空间失败");
//			return;
//		}
//		_capacity = capacity;
//		_size = 0;
//	}
//	void Push(const DataType& data)
//	{
//		// 扩容
//		_array[_size] = data;
//		++_size;
//	}
//	DataType Top()
//	{
//		return _array[_size - 1];
//	}
//	void Destroy()
//	{
//		if (_array)
//		{
//			free(_array);
//			_array = nullptr;
//			_size = _capacity = 0;
//		}
//	}
//private:
//	//成员变量
//	DataType* _array;
//	size_t _capacity;
//	size_t _size;
//};
//int main()
//{
//	Stack s;
//	s.Init(10);
//	s.Push(1);
//	s.Push(2);
//	s.Push(3);
//	cout << s.Top() << endl;
//	s.Push(4);
//	s.Push(5);
//	s.Push(6);
//	s.Push(7);
//	cout << s.Top() << endl;
//	s.Destroy();
//
//	return 0;
//}


//////////////////////////////////////////////////////////////////////////////////////////////////

//#include "Class.h"
//
//int main()
//{
//	Stack st1;
//	Stack st2;
//
//	st1.Init(7);
//	st1.Push(1);
//	st1.Push(2);
//	st1.Push(3);
//	cout << st1.Top() << endl;
//	st1.Destroy();
//
//	return 0;
//}

/////////////////////////////////////////////////////////////////////////////////////////////////

//#include<iostream>
//using namespace std;
////class A
////{
////public:
////	// 健身房 篮球场等等
////	void PrintA()
////	{
////		cout << _a << endl;
////	}
//////private:
////	// 卧室，厨房等等
////	char _a;
////};
////
////int main()
////{
////	A aa1;
////	A aa2;
////	A aa3;
////	cout << sizeof(A) << endl;
////	cout << sizeof(aa1) << endl;
////	aa1._a = 1;
////	aa1.PrintA();
////
////	return 0;
////}
//
//// 类中既有成员变量，又有成员函数
//class A1 {
//public:
//    void f1() {}
//private:
//    char _ch;
//    //int _a;
//    double _d;
//};
//
//// 类中仅有成员函数
//class A2 {
//public:
//    void f2() {}
//};
//
//// 类中什么都没有---空类
//class A3
//{};
//
////int main()
////{
////    //cout << sizeof(A1) << endl;
////
////    // 没有成员变量的类对象，需要1byte，是为了占位，表示对象存在
////    // 不存储有效数据
////    cout << sizeof(A2) << endl;
////    cout << sizeof(A3) << endl;
////    A2 aa1;
////    A2 aa2;
////    cout << &aa1 << endl;
////    cout << &aa2 << endl;
////
////    return 0;
////}




/////////////////////////////////////////////////////////////////////////////////////////////

//this指针

#include<iostream>
using namespace std;

class Date
{
public:
	// this不能在形参和实参显示传递，但是可以在函数内部显示使用
	void Init(int year = 2024,int month = 10,int day = 01)
	{
		this->_year  = year;
		this->_month = month;
		this->_day   = day;
	}

	void Print() //void Print(Date* connst this)
	{
		cout << this->_year << "-" << this->_month << "-" << this->_day << endl;
	}
	/*void Print(Date* const this)
	{
		cout << this->_year << "-" << this->_month << "-" << this->_day << endl;
	}*/

private:
	int _year;
	int _month;
	int _day;
};


int main()
{
	Date d1;
	Date d2;

	d1.Init(2024, 9, 30);
	d1.Print();//d1.Print(&d1);

	d2.Init();
	d2.Print();//d2.Print(&d2);

	return 0;
}







