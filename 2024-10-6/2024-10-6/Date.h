#pragma once
#include<iostream>
using namespace std;

class Date
{
public:
	//构造函数-- 全缺省
	Date(int year = 2024, int month = 10, int day = 6);
	//拷贝构造函数
	Date(const Date& d);
	
	//
	bool operator<(const Date& d);
	bool operator==(const Date& d);
	bool operator<=(const Date& d);
	bool operator>(const Date& d);
	bool operator>=(const Date& d);
	bool operator!=(const Date& d);

	int GetMonthDay(int year, int month);

	//运算符重载
	Date& operator=(const Date& d);

	Date& operator+=(int day);
	Date operator+(int day);

	//前置++
	Date& operator++();
	//后置++
	Date operator++(int);

	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};


