#include<iostream>
using namespace std;
//类的6个默认成员函数
//1. 构造函数
//2. 析构函数
//3. 拷贝构造函数
//4. 赋值运算符重载
//5. const成员函数
//6. 取地址及const取地址操作符重载

//////////////////////////////////////////////////////////////////////////////////////

////一个日期类
//class Date
//{
//public:
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//
//int main()
//{
//	Date d1;
//	d1.Init(2024, 8, 15);
//	d1.Print();
//
//	Date d2;
//	d2.Init(2024, 10, 01);
//	d2.Print();
//
//	return 0;
//}

//////////////////////////////////////////////////////////////////////////////////////

//1.构造函数
//特征:
//1. 函数名与类名相同。
//2. 无返回值。
//3. 对象实例化时编译器自动调用对应的构造函数。
//4. 构造函数可以重载。

//class Date
//{
//public:
//  //无参调用会产生歧义
//	//1.无参构造函数
//	Date()
//	{
//
//	}
//	//2.带参构造函数
//	Date(int year,int month,int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//
//int main()
//{
//	Date d1;//无参函数的调用构造函数
//	d1.Print();
//
//	Date d2(2024, 10, 3);//带参函数的调用构造函数
//	d2.Print();
//
//	// 注意：如果通过无参构造函数创建对象时，对象后面不用跟括号，否则就成了函数声明
//	// 以下代码的函数：声明了d3函数，该函数无参，返回一个日期类型的对象
//    // warning C4930: “Date d3(void)”: 未调用原型函数(是否是有意用变量定义的?)
//	Date d3();
//
//	return 0;
//}

//5. 如果类中没有显式定义构造函数，则C++编译器会自动生成一个无参的默认构造函数，一旦用户显式定义编译器将不再生成。

//class Date
//{
//public:
//	
//	/*// 如果用户显式定义了构造函数，编译器将不再生成
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}*/
//	
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	// 将Date类中构造函数屏蔽后，代码可以通过编译，因为编译器生成了一个无参的默认构造函数
//	// 将Date类中构造函数放开，代码编译失败，因为一旦显式定义任何构造函数，编译器将不再生成
//	// 无参构造函数，放开后报错：error C2512: “Date”: 没有合适的默认构造函数可用
//
//	Date d1;
//	return 0;
//}


//6. 关于编译器生成的默认成员函数，会有疑惑：不实现构造函数的情况下，编译器会
//生成默认的构造函数。但是看起来默认构造函数又没什么用？d对象调用了编译器生成的默
//认构造函数，但是d对象_year / _month / _day，依旧是随机值。也就说在这里编译器生成的
//默认构造函数并没有什么用？？
//解答：C++把类型分成内置类型(基本类型)和自定义类型。内置类型就是语言提供的数据类型，
//如：int / char...，自定义类型就是我们使用class / struct / union等自己定义的类型，
//看看下面的程序，就会发现编译器生成默认的构造函数会对自定类型成员_t调用的它的默认成员函数。

//class Time
//{
//public:
//	Time()
//	{
//		cout << "Time()" << endl;
//		_hour = 0;
//		_minute = 0;
//		_second = 0;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//class Date
//{
//private:
//	// 基本类型(内置类型)
//	int _year;
//	int _month;
//	int _day;
//	// 自定义类型
//	Time _t;
//};
//int main()
//{
//	Date d;
//	return 0;
//}

//注意：C++11 中针对内置类型成员不初始化的缺陷，又打了补丁，即：内置类型成员变量在类中声明时可以给默认值。

//class Time
//{
//public:
//	Time()
//	{
//		cout << "Time()" << endl;
//		_hour = 0;
//		_minute = 0;
//		_second = 0;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//class Date
//{
//public:
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	// 基本类型(内置类型)
//	int _year = 2024;
//	int _month = 10;
//	int _day = 3;
//	// 自定义类型
//	Time _t;
//};
//int main()
//{
//	Date d;
//	d.Print();
//
//	return 0;
//}

//7.无参的构造函数和全缺省的构造函数都称为默认构造函数，并且默认构造函数只能有一个。
//注意：无参构造函数、全缺省构造函数、我们没写编译器默认生成的构造函数，都可以认为是默认构造函数。

//class Date
//{
//public:
//	//1.符合函数重载
//	//2.函数调用的时候产生歧义
//	Date()
//	{
//		_year = 2024;
//		_month = 10;
//		_day = 3;
//	}
//	Date(int year = 2024, int month = 10, int day = 3)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//// 以下测试函数能通过编译吗？
//// 函数调用产生歧义.
//int main()
//{
//	Date d1;
//
//	return 0;
//}

////////////////////////////////////////////////////////////////////////////////////////////////////

//2.析构函数 -- 析构函数不是完成对对象本身的销毁，局部对象销毁工作是由编译器完成的。而对象在销毁时会自动调用析构函数，完成对象中资源的清理工作。
//特征
//1. 析构函数名是在类名前加上字符 ~。
//2. 无参数无返回值类型。
//3. 一个类只能有一个析构函数。若未显式定义，系统会自动生成默认的析构函数。注意：析构函数不能重载
//4. 对象生命周期结束时，C++编译系统系统自动调用析构函数

//typedef int DataType;
//class Stack
//{
//public:
//	Stack(size_t capacity = 3)
//	{
//		_array = (DataType*)malloc(sizeof(DataType) * capacity);
//		if (NULL == _array)
//		{
//			perror("malloc申请空间失败!!!");
//			return;
//		}
//		_capacity = capacity;
//		_size = 0;
//	}
//	void Push(DataType data)
//	{
//		// CheckCapacity();
//		_array[_size] = data;
//		_size++;
//	}
//	// 其他方法...
//	~Stack()
//	{
//		cout << "~Stack()" << endl;
//		if (_array)
//		{
//			free(_array);
//			_array = NULL;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//private:
//	DataType* _array;
//	int _capacity;
//	int _size;
//};
//
//int main()
//{
//	Stack s;
//	s.Push(1);
//	s.Push(2);
//}

//5. 关于编译器自动生成的析构函数，是否会完成一些事情呢？下面的程序我们会看到，编译器生成的默认析构函数，对自定类型成员调用它的析构函数。

class Time
{
public:
	~Time()
	{
		cout << "~Time()" << endl;
	}
private:
	int _hour;
	int _minute;
	int _second;
};
class Date
{
private:
	// 基本类型(内置类型)
	int _year = 2024;
	int _month = 10;
	int _day = 3;
	// 自定义类型
	Time _t;
};

int main()
{
	Date d;
	return 0;
}

// 程序运行结束后输出：~Time()
// 在main方法中根本没有直接创建Time类的对象，为什么最后会调用Time类的析构函数？
// 因为：main方法中创建了Date对象d，而d中包含4个成员变量，其中_year, _month, _day
// 三个是内置类型成员，销毁时不需要资源清理，最后系统直接将其内存回收即可；而_t是Time类对象，
// 所以在d销毁时，要将其内部包含的Time类的_t对象销毁，所以要调用Time类的析构函数。
// 但是main函数中不能直接调用Time类的析构函数，实际要释放的是Date类对象，所以编译器会调用Date类的析构函数，
// 而Date没有显式提供，则编译器会给Date类生成一个默认的析构函数，目的是在其内部调用Time类的析构函数，即当Date对象销毁时，
// 要保证其内部每个自定义对象都可以正确销毁
// main函数中并没有直接调用Time类析构函数，而是显式调用编译器为Date类生成的默认析构函数
// 注意：创建哪个类的对象则调用该类的析构函数，销毁那个类的对象则调用该类的析构函数