#include "Date.h"




int main()
{
	Date d1;
	cout << "d1:";
	cout << d1;

	printf("\n");

	Date d2(2024, 10, 8);
	//Date d3 = d2;
	Date d3(d2);
	cout << "d3:";
	cout << d3;

	printf("\n");

	Date d4;
	d4 = d3 = d1;
	cout << "d4:";
	cout << d4;

	printf("\n");

	Date d5(2024, 10, 7);
	d5 += 100;
	cout << d5;

	printf("\n");

	Date d6(2024, 10, 7);
	Date d7(2024, 10, 7);
	d6 + 100;
	cout << d6;
	d7 = d6 + 100;
	cout << d7;

	printf("\n");

	Date d8(2023, 10, 7);
	Date d9(2024, 10, 8);
	cout << (d8 > d9) << endl;//0
	cout << (d8 >= d9) << endl;//0
	cout << (d8 < d9) << endl;//1
	cout << (d8 <= d9) << endl;//1
	cout << (d8 == d9) << endl;//0
	cout << (d8 != d9) << endl;//1

	printf("\n");

	Date d10;
	Date d11;
	//d11 = ++d10;
	d11 = d10++;
	cout << d10;
	cout << d11;

	printf("\n");

	Date d12(2024, 10, 8);
	d12 -= 100;
	cout << d12;

	printf("\n");

	Date d13(2024, 10, 8);
	d13 -= -100;
	cout << d13;

	printf("\n");

	Date d14(2024, 10, 8);
	d14 += -100;
	cout << d14;

	printf("\n");

	Date d15(2024, 10, 8);
	d15 - 100;
	cout << d15;
	Date d16;
	d16 = d15 - 100;
	cout << d16;

	printf("\n");

	Date d17(2024, 10, 8);
	Date d18;
	d18 = d17 + -100;
	cout << d18;

	printf("\n");

	Date d19(2024, 10, 8);
	Date d20 = --d19;
	cout << d19;
	cout << d20;
	Date d21(2024, 10, 8);
	Date d22 = d21--;
	cout << d21;
	cout << d22;

	printf("\n");

	Date d23(2024, 10, 8);
	Date d24(1949, 10, 1);
	cout << (d23 - d24) << endl;


	Date d25(2024, 10, 9);

	cout << d25;

	Date d26;
	cin >> d26;
	cout << d26;


	return 0;
}