//#include<iostream>
//using namespace std;

//3.拷贝构造函数
//拷贝构造函数：只有单个形参，该形参是对本类类型对象的引用(一般常用const修饰)，在用已存
//在的类类型对象创建新对象时由编译器自动调用。

//特征
//拷贝构造函数也是特殊的成员函数，其特征如下：
//1. 拷贝构造函数是构造函数的一个重载形式。
//2. 拷贝构造函数的参数只有一个且必须是类类型对象的引用，使用传值方式编译器直接报错，因为会引发无穷递归调用。

//class Date
//{
//public:
//	//构造函数
//	Date(int year = 2024,int month = 10,int day = 3)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	//拷贝构造
//	//Date(const Date d)//错误的写法:编译报错，会引发无穷的递归
//	Date(const Date& d)//正确的写法
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//
//int main()
//{
//	Date d1;
//	d1.Print();
//
//	Date d2(2024,10,5);
//	d2.Print();
//
//	Date d3(d2);
//	d3.Print();
//
//	return 0;
//}

//3. 若未显式定义，编译器会生成默认的拷贝构造函数.默认的拷贝构造函数对象按内存存储按字节序完成拷贝，这种拷贝叫做浅拷贝，或者值拷贝。

//class Time
//{
//public:
//	Time(int hour = 1, int minute = 1,int second = 1)
//	{
//		_hour = hour;
//		_minute = minute;
//		_second = second;
//	}
//	Time(const Time& t)
//	{
//		_hour = t._hour;
//		_minute = t._minute;
//		_second = t._second;
//		cout << "Time(const Time& t)" << endl;
//		cout << _hour << ":" << _minute << ":" << _second << endl;
//	}
//		
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//public:
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	//基本类型(内置类型)
//	int _year = 2024;
//	int _month = 10;
//	int _day = 5;
//
//	//自定义类型
//	Time _time;
//};
//
//int main()
//{
//	Date d1;
//	// 用已经存在的d1拷贝构造d2，此处会调用Date类的拷贝构造函数
//    // 但Date类并没有显式定义拷贝构造函数，则编译器会给Date类生成一个默认的拷贝构造函数
//	Date d2(d1);
//	d2.Print();
//
//	return 0;
//}

//注意：在编译器生成的默认拷贝构造函数中，内置类型是按照字节方式直接拷贝的，而自定义类型是调用其拷贝构造函数完成拷贝的。

//4. 编译器生成的默认拷贝构造函数已经可以完成字节序的值拷贝了，还需要自己显式实现吗？当然像日期类这样的类是没必要的。
//那么下面的类呢？验证一下试试？
//class Stack
//{
//public:
//	Stack(int capacity = 10)
//	{
//		_a = (int*)malloc(sizeof(int) * capacity);
//		if (_a == nullptr)
//		{
//			perror("Stack->malloc申请空间失败\n");
//			return;
//		}
//		int _top = 0;
//		int _capacity = capacity;
//	}
//
//	void Push(int x)
//	{
//		//checkcapacity()
//		_a[_top++] = x;
//	}
//
//	~Stack()
//	{
//		if (_a)
//		{
//			free(_a);
//			_top = _capacity = 0;
//		}
//	}
//
//private:
//	int* _a;
//	int _top;
//	int _capacity;
//};
//
//int main()
//{
//	Stack st1;
//	st1.Push(1);
//	st1.Push(2);
//	st1.Push(3);
//	
//	//1.st1对象调用构造函数创建，在构造函数中，默认申请了10个元素的空间然后里面存了3个元素123
//	//2.st2对象使用st1拷贝构造，而Stack类没有显式定义拷贝构造函数，则编译器会给Stack类生成一份默认的拷贝构造函数，
//	//	默认拷贝构造函数是按照值拷贝的，即将st1中内容原封不动的拷贝到st2中。因此s1和s2指向了同一块内存空间。
//	//3.当程序退出时，st2和st1要销毁。st2先销毁，st2销毁时调用析构函数，已经将申请的空间释放了，但是st1并不知道，
//	//  到st1销毁时，会将申请的空间再释放一次，一块内存空间多次释放，肯定会造成程序崩溃。
//	Stack st2(st1);
//
//	return 0;
//}

//注意：类中如果没有涉及资源申请时，拷贝构造函数是否写都可以；一旦涉及到资源申请时，则拷贝构造函数是一定要写的，否则就是浅拷贝。






//////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include<iostream>
using namespace std;

//4.赋值运算符重载
//注意：
//不能通过连接其他符号来创建新的操作符：比如operator@
//重载操作符必须有一个类类型参数
//用于内置类型的运算符，其含义不能改变，例如：内置的整型 + ，不 能改变其含义
//作为类成员函数重载时，其形参看起来比操作数数目少1，因为成员函数的第一个参数为隐
//藏的this
//.*   ::   sizeof   ?:  .   注意以上5个运算符不能重载。这个经常在笔试选择题中出现。

//class Date
//{
//public:
//	Date(int year = 2024,int month = 10,int day = 5)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	// 这里需要注意的是，左操作数是this，指向调用函数的对象
//	bool operator==(const Date& d)
//	{
//		return _year == d._year
//			&& _month == d._month
//			&& _day == d._day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//
//
//int main()
//{
//	Date d1(2024, 10, 3);
//	Date d2(2024, 10, 5);
//	cout << (d1 == d2) << endl;
//
//	return 0;
//}

//赋值运算符重载
//1.赋值运算符重载格式
//参数类型：const T&,传递引用可以提高传参效率
//返回值类型：T&,返回引用可以提高返回的效率，有返回值目的是为了支持连续赋值
//检测是否自己给自己赋值
//返回*this:要复合连续赋值的含义

//class Date
//{
//public:
//	//构造函数
//	Date(int year = 2024, int month = 10, int day = 5)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	//拷贝构造
//	Date(const Date& d)
//	{
//		cout << "Date(const Date& d)" << endl;
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//	//赋值运算符重载
//	Date& operator=(const Date& d)
//	{
//		/*_year = d._year;
//		_month = d._month;
//		_day = d._day;*/
//		this->_year = d._year;
//		this->_month = d._month;
//		this->_day = d._day;
//		return *this;
//	}
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1;
//	Date d2(2024,10,6);
//	// 用一个已经存在的对象初始化另一个对象   -- 构造函数
//	Date d3(d2);
//	d3.Print();
//	// 已经存在的两个对象之间复制拷贝        -- 运算符重载函数
//	d3 = d2 = d1;
//	d1.Print();
//
//	return 0;
//}


//2. 赋值运算符只能重载成类的成员函数不能重载成全局函数
//原因：赋值运算符如果不显式实现，编译器会生成一个默认的。此时用户再在类外自己实现
//一个全局的赋值运算符重载，就和编译器在类中生成的默认赋值运算符重载冲突了，故赋值
//运算符重载只能是类的成员函数。


//3.用户没有显式实现时，编译器会生成一个默认赋值运算符重载，以值的方式逐字节拷贝。
//注意：内置类型成员变量是直接赋值的，而自定义类型成员变量需要调用对应类的赋值运算符重载完成赋值。

//class Time
//{
//public:
//	Time(int hour = 12,int minute = 12,int second = 12)
//	{
//		_hour = hour;
//		_minute = minute;
//		_second = second;
//	}
//
//	Time& operator=(const Time& t)
//	{
//		if (this != &t)
//		{
//			cout << "Time& operator=(const Time& t)" << endl;
//			_hour = t._hour;
//			_minute = t._minute;
//			_second = t._second;
//		}
//		return *this;
//	}
//
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//public:
//	//基本类型(内置类型)
//	int _year = 2024;
//	int _month = 10;
//	int _day = 5;
//	//自定义类型
//	Time _t;
//};
//
//int main()
//{
//	Date d1;
//	Date d2;
//
//	//d1 = d2;
//	d1 = d1;
//
//	return 0;
//}

//既然编译器生成的默认赋值运算符重载函数已经可以完成字节序的值拷贝了，
//还需要自己实现吗？当然像日期类这样的类是没必要的。那么下面的类呢？验证一下试试？

// 这里会发现下面的程序会崩溃掉？这里就需要我们以后讲的深拷贝去解决。
//typedef int DataType;
//class Stack
//{
//public:
//	Stack(size_t capacity = 10)
//	{
//		_array = (DataType*)malloc(capacity * sizeof(DataType));
//		if (nullptr == _array)
//		{
//			perror("malloc申请空间失败");
//			return;
//		}
//		_size = 0;
//		_capacity = capacity;
//	}
//	void Push(const DataType& data)
//	{
//		// CheckCapacity();
//		_array[_size] = data;
//		_size++;
//	}
//	~Stack()
//	{
//		if (_array)
//		{
//			free(_array);
//			_array = nullptr;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//private:
//	DataType* _array;
//	size_t _size;
//	size_t _capacity;
//};
//
//int main()
//{
//	Stack s1;
//	s1.Push(1);
//	s1.Push(2);
//	s1.Push(3);
//	s1.Push(4);
//
//	Stack s2;
//	s2 = s1;
//	return 0;
//}

//注意：如果类中未涉及到资源管理，赋值运算符是否实现都可以；一旦涉及到资源管理则必须要实现。

//1.s1对象调用构造函数创建，在构造函数中默认申请了10个元素的空间，然后存了4个元素1234
//2.s2对象调用构造函数创建，在构造函数中默认申请了10个元素的空间，没有存储元素
//3.由于Stack没有显式实现赋值运算符重载，编译器会以浅拷贝的方式实现一份默认的赋值运算符重载
//即只要发现Stack的对象之间相互赋值，就会将一个对象中内容原封不动拷贝到另一个对象中
//4.s2 = s1:当s1给s2赋值时，编译器会将s1中内容原封不动拷贝到s2中，这样会导致两个问题:
//a.s2原来的空间丢失了，存在内存泄漏
//b.s1和s2共享同一份内存空间，最后销毁时会导致同一份内存空间释放两次而引起程序崩溃






