#pragma once

#include<stdio.h>
#include<stdlib.h>

typedef int StackDataType;

struct Stack
{
	StackDataType* a;
	int top;
	int capacity;
};

void StackInit(struct Stack* pst, int defaultcapacity = 4);
void StackPush(struct Stack* pst, int x);
void StackPush(struct Stack* pst, double x);

