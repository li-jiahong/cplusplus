#include "Stack.h"

void StackInit(struct Stack* pst, int defaultcapacity)
{
	pst->a = (int*)malloc(sizeof(int) * defaultcapacity);
	if (pst->a == NULL)
	{
		perror("malloc fail\n");
		return;
	}

	pst->top = 0;
	pst->capacity = defaultcapacity;
}

void StackPush(struct Stack* pst, int x)
{
	printf("void StackPush(struct stack* pst, int x)\n");
}

void StackPush(struct Stack* pst, double x)
{
	printf("void StackPush(struct stack* pst, double x)\n");
}


