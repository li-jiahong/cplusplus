#include<iostream>

using namespace std;

////缺省参数
//void func(int a = 7)
//{
//	cout << a << endl;
//}
//
//int main()
//{
//	func();  //没有传参时，使用默认参数
//	func(3); //传参时，使用指定参数
//
//	return 0;
//}


////1.全缺省参数
//void func(int a = 10,int b = 20,int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl << endl;
//}
//
//int main()
//{
//
//	func();
//	func(1);
//	func(1,2);
//	func(1,2,3);
//
//	return 0;
//}



////半缺省参数
////1. 半缺省参数必须从右往左依次来给出，不能间隔着给
//void func(int a , int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl << endl;
//}
//
//int main()
//{
//	func(1);
//	func(1,2);
//	func(1,2,3);
//
//	return 0;
//}


//////////////////////////////////////////////////////////////

//#include <stdlib.h>
//
//struct Stack
//{
//	int* a;
//	int top;
//	int capacity;
//};
//
////#define INIT_CAPACITY 4
////void StackInit(struct Stack* pst)
////{
////	pst->a = (int*)malloc(sizeof(int) * INIT_CAPACITY);
////	if (pst->a == NULL)
////	{
////		perror("malloc fail\n");
////		return;
////	}
////
////	pst->top = 0;
////	pst->capacity = INIT_CAPACITY;
////}
//
//
//void StackInit(struct Stack* pst,int INIT_CAPACITY = 4)
//{
//	pst->a = (int*)malloc(sizeof(int) * INIT_CAPACITY);
//	if (pst->a == NULL)
//	{
//		perror("malloc fail\n");
//		return;
//	}
//
//	pst->top = 0;
//	pst->capacity = INIT_CAPACITY;
//}
//
//
//int main()
//{
//	// 插入70个数据
//	struct Stack st1;
//	StackInit(&st1,70);
//
//	// 不知道插入多少个数据
//	struct Stack st2;
//	StackInit(&st2);
//
//	return 0;
//}



////////////////////////////////////////////////////////////////

//函数重载：是函数的一种特殊情况，C++允许在同一作用域中声明几个功能类似的同名函数，
//这些同名函数的形参列表(参数个数 或 类型 或 类型顺序)不同


////1、参数类型不同
//int Add(int left,int right)
//{
//	cout << "int Add(int left,int right)" << endl;
//	return left + right;
//}
//
//double Add(double left, double right)
//{
//	cout << "double Add(double left, double right)" << endl;
//	return left + right;
//}
//
//int main()
//{
//	cout << Add(1, 1) << endl;
//	cout << Add(1.1, 1.1) << endl;
//
//	return 0;
//}



////2.参数个数不同
//void f()
//{
//	cout << "f()" << endl;
//}
//
//void f(int a)
//{
//	cout << "f(int a)" << endl;
//}
//
//int main()
//{
//	f();
//	f(0);
//
//	return 0;
//}


//// 3、参数类型顺序不同
//void f(int a, double b)
//{
//	cout << "void f(int a, double b)" << endl;
//}
//
//void f(double a, int b)
//{
//	cout << "void f(double a, int b)" << endl;
//}
//
//int main()
//{
//	f(1, 2.2);
//	f(1.1, 2);
//
//	return 0;
//}


// 1、是否构成重载 -- 构成
// 2、问题。无参调用存在歧义
//void f()
//{
//	cout << "f()" << endl;
//}
//
//void f(int a = 0)
//{
//	cout << "f(int a)" << endl;
//}
//
//int main()
//{
//	f(); // “f”: 对重载函数的调用不明确
//
//	return 0;
//}

//// 为什么C语言不支持，Cpp支持重载，CPP怎么支持的呢
//// 编译链接过程
//// 函数名修饰规则
//
//#include "Stack.h"
//
//
//int main()
//{
//	struct Stack st;
//	StackInit(&st);
//	StackPush(&st, 1);
//	StackPush(&st, 1.1);
//
//	return 0;
//}



//////////////////////////////////////////////////////////////////////

//引用--给已存在变量取了一个别名

//int main()
//{
//	int a = 0;
//	int& b = a;
//	int& c = b;
//	int& d = a;
//
//
//	cout << &a << endl;
//	cout << &b << endl;
//	cout << &c << endl;
//	cout << &d << endl;
//
//	b++;
//	d++;
//
//	cout << a << endl;
//	cout << b << endl;
//	cout << c << endl;
//	cout << d << endl;
//
//
//	return 0;
//}

////1. 做参数
//void Swap(int& a, int& b)
//{
//	int tmp = a;
//	a = b;
//	b = tmp;
//}
//
//void Swap(int*& a, int*& b)
//{
//	int tmp = *a;
//	*a = *b;
//	*b = tmp;
//}
//
//int main()
//{
//	int x = 0, y = 1;
//	cout << "交换前" << endl;
//	cout << "x = " << x << " " << "y = " << y << endl;
//	Swap(x, y);
//	cout << "交换后" << endl;
//	cout << "x = " << x << " " << "y = " << y << endl;
//
//
//	int* px = &x, * py = &y;
//	cout << "交换前" << endl;
//	cout << "x = " << x << " " << "y = " << y << endl;
//	Swap(px, py);
//	cout << "交换后" << endl;
//	cout << "x = " << x << " " << "y = " << y << endl;
//
//	return 0;
//}



//引用不是新定义一个变量，而是给已存在变量取了一个别名，编译器不会为引用变量开辟内存空
//间，它和它引用的变量共用同一块内存空间。


////2. 做返回值
//int& Count()
//{
//	static int a = 0;
//	a++;
//	return a;
//}
//
//
//int main()
//{
//	int a = Count();
//	cout << a << endl;
//
//	return 0;
//}

////如果函数返回时，出了函数作用域，如果返回对象还在(还没还给系统)，则可以使用
////引用返回，如果已经还给系统了，则必须使用传值返回。
//int& Add(int a, int b)
//{
//	int c = a + b;
//	return c;
//}
//int main()
//{
//	int& ret = Add(1, 2);
//	Add(3, 4);
//	cout << "Add(1, 2) is :" << ret << endl;
//	return 0;
//}





////引用和指针的区别
//// 
////在语法概念上引用就是一个别名，没有独立空间，和其引用实体共用同一块空间。
//int main()
//{
//	int a = 10;
//	int& ra = a;
//	cout << "&a = " << &a << endl;
//	cout << "&ra = " << &ra << endl;
//	return 0;
//}

//在底层实现上实际是有空间的，因为引用是按照指针方式来实现的。

//引用和指针的不同点:
//1. 引用概念上定义一个变量的别名，指针存储一个变量地址。
//2. 引用在定义时必须初始化，指针没有要求
//3. 引用在初始化时引用一个实体后，就不能再引用其他实体，而指针可以在任何时候指向任何一个同类型实体
//4. 没有NULL引用，但有NULL指针
//5. 在sizeof中含义不同：引用结果为引用类型的大小，但指针始终是地址空间所占字节个数(32位平台下占4个字节)
//6. 引用自加即引用的实体增加1，指针自加即指针向后偏移一个类型的大小
//7. 有多级指针，但是没有多级引用
//8. 访问实体方式不同，指针需要显式解引用，引用编译器自己处理
//9. 引用比指针使用起来相对更安全


//auto关键字(C++11)

//int TestAuto()
//{
//	return 10;
//}
//int main()
//{
//	int a = 1;
//	auto b = a;
//	auto c = 'b';
//	auto d = TestAuto();
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//
//	//auto e;//无法通过编译,使用auto定义变量时必须对其进行初始化
//	return 0;
//}

//使用auto定义变量时必须对其进行初始化，在编译阶段编译器需要根据初始化表达式来推导auto
//的实际类型。因此auto并非是一种“类型”的声明，而是一个类型声明时的“占位符”，编译器在编
//译期会将auto替换为变量实际的类型。


//1. auto与指针和引用结合起来使用
//用auto声明指针类型时，用auto和auto*没有任何区别，但用auto声明引用类型时则必须加&
//int main()
//{
//	int   x = 10;
//	auto  a = &x;
//	auto* b = &x;
//	auto& c = x;
//	cout << typeid(a).name() << endl;
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	
//	return 0;
//}

