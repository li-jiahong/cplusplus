//#include<iostream>
//#include<stdlib.h>
//
////命名空间
//namespace ljh
//{
//	int rand = 0;
//}
//
//using std::cout;
//using std::endl;
//
//int main()
//{
//	cout << ljh::rand << endl;
//
//	printf("%d\n", ljh::rand);
//
//	return 0;
//}



//#include<iostream>
//
//using namespace std;
//
//namespace ljh
//{
//	int a = 3;
//	namespace jh
//	{
//		int a = 4;
//	}
//}
////using namespace ljh;
//
//int a = 2;
//
////局部域 -> 全局域 -> 展开命名空间域 or 指定访问命名空间域
//int main()
//{
//	int a = 1;
//
//	cout << ljh::a << endl;
//	cout << a << endl;
//	cout << ljh::jh::a << endl;
//
//	//printf("\n");
//	cout << endl;
//	int b = 0;
//	cin >> b;
//	cout << b << endl;
//
//	return 0;
//}

 // 可以自动识别变量的类型 cin、cout
#include<iostream>

using namespace std;

int main()
{
	int a;
	float b;
	double c;
	char d;

	cin >> a >> b >> c >> d;

	cout << a << " " << b << " " << c << " " << d << endl;

	return 0;
}