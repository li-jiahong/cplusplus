#include<stdio.h>

//int a = 0;
//
////类域
////命名空间域
//
////局部域
////全局域
//
//int main()
//{
//	int a = 1;
//
//	printf("%d\n", a);
//
//	// ::域作用限定域
//	printf("%d\n", ::a);
//
//	return 0;
//}


//int a = 0;
//
//namespace ljh
//{
//	int a = 1;
//}
//
////局部域 -> 全局域 -> 展开命名空间域 or 指定访问命名空间域
//
//int main()
//{
//	int a = 2;
//
//	printf("%d\n", a);
//	printf("%d\n", ::a);
//	printf("%d\n", ljh::a);
//
//	return 0;
//}


//int a = 0;
//
//namespace ljh
//{
//	int a = 1;
//}
//
//using namespace ljh;
//
////局部域 -> 全局域 -> 展开命名空间域 or 指定访问命名空间域
//
//int main()
//{
//	//重定义错误
//	printf("%d\n", a);
//
//	return 0;
//}


//#include<stdlib.h>
//
//namespace ljh
//{
//	int rand = 1;
//}
//
//int main()
//{
//	printf("%d\n", ljh::rand);
//	printf("%d\n", rand);
//
//	return 0;
//}


//namespace N1
//{
//	int a = 0;
//	int b = 1;
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//	namespace N2
//	{
//		int a = 1;
//		int b = 2;
//		int Sub(int left, int right)
//		{
//			return left - right;
//		}
//	}
//}
//
//int main()
//{
//	printf("%d\n", N1::a);
//	printf("%d\n", N1::N2::a);
//	printf("%d\n", N1::b);
//	printf("%d\n", N1::N2::b);
//	printf("%d\n", N1::Add(1, 2));
//	printf("%d\n", N1::N2::Sub(3,2));
//
//
//	return 0;
//}


//#include<iostream>
//
//int main()
//{
//	std::cout << "hello CPP" << std::endl;
//	std::cout << "hello CPP" << std::endl;
//	std::cout << "hello CPP" << std::endl;
//	std::cout << "hello CPP" << std::endl;
//
//
//	return 0;
//}



//#include<iostream>
//
//using namespace std;
//// 直接展开会有风险，我们定义如果跟库重名，就报错了
//// 建议项目里面不要去展开，建议日常练习可以这么玩
//// 项目建议指定访问，不要轻易展开命名空间
//
//int main()
//{
//	cout << "hello CPP" << endl;
//	cout << "hello CPP" << endl;
//	cout << "hello CPP" << endl;
//	cout << "hello CPP" << endl;
//
//	return 0;
//}





//#include<iostream>
//
//// 展开某个：把常用展开
//using std::cout;
//using std::endl;
//
//int main()
//{
//	cout << "hello CPP" << endl;
//	cout << "hello CPP" << endl;
//	cout << "hello CPP" << endl;
//	cout << "hello CPP" << endl;
//
//
//	return 0;
//}


#include<iostream>

using std::cout;//流插入运算符
using std::cin;
using std::endl;

int main()
{
	int a = 1;
	double b = 11.11;

	//自动识别类型
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;

	// >> 流提取运算符
	cin >> a >> b;
	cout << a << endl << b << endl;

	return 0;
}

